{
    'name': "Lsv Retentions module",
    'author': "Lsv Tech",
    'version': "1.0.0-dev1",
    'summary': "this is a module to manage retentions",
    'depends': ['contacts', 'sale_management', 'account'],
    'data': [        
        'views/account_move_views.xml',
    ],

}