"""
this is a python module to manage the lsv.print.account_move module
"""
import logging
from odoo import models, fields

class AccountMove(models.Model):
    """
    lsv.print.account_move module with all attributes and logic.
    """
    _name = 'account.move'
    _inherit = 'account.move'
    _description = 'Account_move model to manage account_move info.'

    additional_information = fields.Text(string="Additional information", required=False)
    last_print_date = fields.Datetime(string="Last print date", required=False)
    retentions_ids = fields.One2many('lsv.print.retentions', 'move_id', string="Retentions")
    reten_percent = fields.Float('retentions(%)', required=True)

   