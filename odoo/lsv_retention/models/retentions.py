"""
this module is used to assing the retentions every invoice.
"""

from  odoo  import models, fields

class Retentions(models.Model):
    """
    
    """
    _name="lsv.print.retentions"
    _description="Retentions of the clients"

    is_aplicated = fields.Boolean(default=False)
    percentage = fields.Float()
    amount_tope = fields.Float()
    move_id = fields.Many2one('account.move', string="Move Id")
    percentage_move = fields.Float('Retentions(%)',
                                    related_name="move_id.reten_percent",
                                    inverse="_inverse_retentions_per")
    
    def _inverse_retentions_per(self):
        """
        Inverse the retentions_percentage.
        """
        for record in self:
            if record.move_id:
                record.move_id.reten_percent = record.percentage